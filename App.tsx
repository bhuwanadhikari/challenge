import { StatusBar, StyleSheet, Text, View } from "react-native";
import { items } from "./src/constants/constants";
import { MyComponent } from "./src/components/MyComponent";

export default function App() {
  return (
    <View style={styles.container}>
      <StatusBar />
      <MyComponent data={items} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 40,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
