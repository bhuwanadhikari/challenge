export const items = [
  { name: "Apple", id: "1" },
  { name: "Xiaomi", id: "2" },
  { name: "Lenovo", id: "3" },
  { name: "Dell", id: "4" },
  { name: "Acer", id: "5" },
  { name: "Asus", id: "6" },
  { name: "HP", id: "7" },
  { name: "Microsoft", id: "8" },
  { name: "Samsung", id: "9" },
  { name: "Huawei", id: "10" },
];
