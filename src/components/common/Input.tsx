import { Ref, forwardRef } from "react";
import { StyleSheet, TextInput, TextInputProps } from "react-native";

export const Input = forwardRef(
  (props: TextInputProps, ref: Ref<TextInput>) => {
    return <TextInput style={styles.input} ref={ref} {...props} />;
  }
);

const styles = StyleSheet.create({
  input: {
    padding: 8,
    borderColor: "#ddd",
    borderRadius: 4,
  },
});
