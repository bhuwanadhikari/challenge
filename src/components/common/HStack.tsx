import { StyleSheet, View, ViewStyle } from "react-native";

type Props = {
  children: React.ReactNode;
} & ViewStyle;

export const HStack = ({ children, ...rest }: Props) => {
  return <View style={[styles.container, { ...rest }]}>{children}</View>;
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
  },
});
