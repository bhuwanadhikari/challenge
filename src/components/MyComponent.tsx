import React, { useState, useEffect, useRef } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  FlatList,
  TextInput,
} from "react-native";
import { Item } from "../types/types";

type Props = {
  data: Item[];
};
export const MyComponent = ({ data }: Props) => {
  const [selectedItems, setSelectedItems] = useState<Item[]>([]);
  const [dataSource, setDataSource] = useState<Item[]>([]);
  const [searchTerm, setSearchTerm] = useState("");
  const inputRef = useRef<TextInput>(null);
  useEffect(() => {
    setDataSource(data);
  }, [data]);
  useEffect(() => {
    setTimeout(() => {
      setDataSource(data.filter((item) => item.name.includes(searchTerm)));
    }, 1000);
  }, [searchTerm]);
  const handleSelect = (item: Item) => {
    setSelectedItems((currentSelectedItems) => [...currentSelectedItems, item]);
  };
  const handleClear = () => {
    inputRef?.current?.clear();
  };
  return (
    <View>
      <TextInput
        ref={inputRef}
        onChangeText={setSearchTerm}
        value={searchTerm}
      />
      <TouchableOpacity onPress={handleClear}>
        <Text>Clear</Text>
      </TouchableOpacity>
      <FlatList
        data={dataSource}
        keyExtractor={(item) => item.id}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity onPress={() => handleSelect(item)}>
              <Text>{item.name}</Text>
              <Text>
                {selectedItems.includes(item) ? "Selected" : "Not selected"}
              </Text>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};
